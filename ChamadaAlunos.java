import java.text.SimpleDateFormat;
import java.util.*;

public class Principal {
    public static void main(String[] args){

        Calendar data = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        List alunos = new ArrayList<String>();

        alunos.add("Giovane");
        alunos.add("Isabela");
        alunos.add("Marcos");
        alunos.add("André");
        alunos.add("Julia");
        alunos.add("Carmen");
        alunos.add("Marcelo");
        alunos.add("Carlos");
        alunos.add("Amanda");
        alunos.add("Carina");

        List alunosPresentes = new ArrayList<String>();
        List alunosAusentes = new ArrayList<String>();

        for(int q = 0; q < alunos.size(); q++ ){
            Scanner ler = new Scanner(System.in);
            System.out.println("O(a) aluno(a) " + alunos.get(q) + " está presente? 's' ou 'n'");
            var presensa = ler.nextLine();

            if(presensa.equals("s")) {
                alunosPresentes.add(alunos.get(q));
            }
            if (presensa.equals("n")) {
                alunosAusentes.add(alunos.get(q));
            }
        }
        System.out.println(sdf.format(data.getTime()));
        System.out.println("Alunos presentes " + alunosPresentes);
        System.out.println("Alunos ausentes " + alunosAusentes);
    }
}
